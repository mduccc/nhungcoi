package bai3;

public class Ford extends Car {
    private int year;
    private int manufacturerDiscount;

    Ford(int year, int manufacturerDiscount, int speed, double regularPrice, String color){
        super(speed, regularPrice , color);
        this.year = year;
        this.manufacturerDiscount = manufacturerDiscount;
    }

    @Override
    public double getSalePrice() {
        return super.getSalePrice() - ((super.getSalePrice() / 100) * manufacturerDiscount);
    }
}
