package bai3;

public class Car {
    private int speed;
    private double regularPrice;
    private String color;

    Car() {
    }

    Car(int speed, double regularPrice, String color) {
        this.speed = speed;
        this.regularPrice = regularPrice;
        this.color = color;
    }

    public double getSalePrice() {
        return regularPrice;
    }

    public int getSpeed() {
        return speed;
    }

    public String getColor() {
        return color;
    }
}


