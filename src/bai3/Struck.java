package bai3;

public class Struck extends Car {
    private int weight;

    Struck(int weight){
        this.weight = weight;
    }

    @Override
    public double getSalePrice() {
        return weight > 2000 ? super.getSalePrice() - ((super.getSalePrice() / 100) * 10) : super.getSalePrice() -
                ((super.getSalePrice() / 100) * 20);
    }
}
