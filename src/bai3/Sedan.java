package bai3;

import java.util.ArrayList;

public class Sedan extends Car {
    private int length;

    Sedan(int length, int speed, double regularPrice, String color) {
        super(speed, regularPrice, color);
        this.length = length;
    }

    @Override
    public double getSalePrice() {
        return length > 20 ? super.getSalePrice() - ((super.getSalePrice() / 100) * 5) : super.getSalePrice() -
                ((super.getSalePrice() / 100) * 10);
    }
}
