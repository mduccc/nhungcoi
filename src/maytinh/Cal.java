package maytinh;

import java.util.Scanner;

public class Cal {
    Scanner keyboard = new Scanner(System.in);

    public double calculate(double a, double b, String operator) {
        double result = 0;
        if (operator.equals("+"))
            result = a + b;
        if (operator.equals("*"))
            result = a * b;
        if (operator.equals("/"))
            result = a / b;
        if (operator.equals("-"))
            result = a - b;
        if (operator.equals("^"))
            result = Math.pow(a, b);

        return result;
    }

    public double inputNumber(String label) {
        double input = 0;
        boolean isError;
        do {
            try {
                System.out.print(label + ": ");
                input = keyboard.nextDouble();
                isError = false;
            } catch (Exception e) {
                System.out.println("Please type number and not null");
                isError = true;
                keyboard.nextLine();
            }
        } while (isError);
        return input;
    }

    public String inputString(String label) {
        System.out.print(label + ": ");
        String input = "";
        boolean isError;
        do {
            input = keyboard.nextLine();
            isError = false;
            if (!input.equals("+") && !input.equals("*") && !input.equals("/") && !input.equals("-")) {
                System.out.println("Please input (+, -, *, /, ^)");
                isError = true;
            }
        } while (isError);
        return input;
    }

    public static void main(String[] args) {
        double result;
        int n;
        double a, b;
        String operator;
        Cal cal = new Cal();

        System.out.print("n = ");
        n = cal.keyboard.nextInt();
        a = cal.inputNumber("Enter number a");
        cal.keyboard.nextLine();
        operator = cal.inputString("Enter operator (+, -, *, /, ^)");
        b = cal.inputNumber("Enter number b");
        if (operator.equals("/")) {
            while (b == 0) {
                System.out.println("Cannot div by 0");
                b = cal.inputNumber("Enter number b");
            }
        }
        result = cal.calculate(a, b, operator);
        if (n == 1)
            System.out.println("Result: " + result);
        else
            System.out.println("Memory: " + result);

        for (int i = 0; i < n - 1; i++) {
            cal.keyboard.nextLine();
            operator = cal.inputString("Enter operator (+, -, *, /, ^)");
            a = cal.inputNumber("Enter number");
            if (operator.equals("/")) {
                while (a== 0) {
                    System.out.println("Cannot div by 0");
                    a= cal.inputNumber("Enter number");
                }
            }
            result = cal.calculate(result, a, operator);

            if (i < n - 1 - 1)
                System.out.println("Memory: " + result);
            else
                System.out.println("Result: " + result);
        }
    }
}